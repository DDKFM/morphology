package de.ddkfm.application;
	
import java.io.File;
import java.util.Optional;
import java.util.Stack;

import javafx.animation.Animation.Status;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Duration;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.fxml.FXMLLoader;


public class Main extends Application {
	private Morphing morph = new Morphing();
	private ImageView imgView = new ImageView(morph.getImage());
	private Stack<Image> stack = new Stack<>();
	private Button btDilatation = new Button("Dilatation");
	private Button btErosion = new Button("Erosion");
	private Button btMarkBorders = new Button("nur R�nder");
	private Button btLoadImage = new Button("Bild laden");
	private Button btRandImage = new Button("Zuf�lliges Bild");
	private Button btGenerateLandscape = new Button("Landschaft generieren");
	
	private int timerIndex = 0;
	
	private Button btBack = new Button("Schritt zur�ck");
	@Override
	public void start(Stage primaryStage) {
		try {
			BorderPane root = new BorderPane();//(BorderPane)FXMLLoader.load(getClass().getResource("Morpo.fxml"));
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			root.setCenter(imgView);
			root.setRight(new VBox(btLoadImage,btRandImage,btErosion,btDilatation,btMarkBorders,btGenerateLandscape));
			root.setTop(btBack);
			addButtonListender();
			imgView.setFitWidth(500);
			imgView.setFitHeight(500);
			imgView.setSmooth(false);
			stack.push(imgView.getImage());
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	private void addButtonListender(){
		btErosion.setOnAction(e->{
			morph.erosion();
			addImage();
		});
		btErosion.setTooltip(new Tooltip("Tr�gt eine Randebene des eines Objektes ab"));
		btDilatation.setOnAction(e->{
			morph.dilation();
			addImage();
		});
		btDilatation.setTooltip(new Tooltip("F�gt eine Randebene zum Objekt hinzu"));
		btMarkBorders.setOnAction(e->{
			morph.onlyBorder();
			addImage();
		});
		btMarkBorders.setTooltip(new Tooltip("l�scht alle Nicht-Rand-Pixel(Nur Rand)"));
		btLoadImage.setOnAction(e->{
			FileChooser fc = new FileChooser();
			fc.setInitialFileName(".");
			String filename = null;
			try {
				filename = fc.showOpenDialog(null).toURL().toString();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			morph.setImage(new Image(filename));
			addImage();
		});
		btLoadImage.setTooltip(new Tooltip("L�dt ein bin�res Bild"));
		btRandImage.setOnAction(e->{
			TextInputDialog dialog = new TextInputDialog();
			dialog.setTitle("zuf�lliges Bild erzeugen(SW-Rauschen)");
			dialog.setContentText("Wahrscheinlichkeit f�r Schwarz in %:");
			Optional<String> result = dialog.showAndWait();
			if(result.isPresent()){
				double percent = Double.parseDouble(result.get());
				morph.generateRandomImage(percent);
				addImage();
			}
		});
		btRandImage.setTooltip(new Tooltip("Generiert ein zuf�lliges Bild(mit bestimmter Wahrscheinlichkeit)"));
		btGenerateLandscape.setOnAction(e->{
			TextInputDialog dialog = new TextInputDialog();
			dialog.setTitle("Landschaft generieren");
			dialog.setContentText("Anzahl der Iterationen");
			Optional<String> result = dialog.showAndWait();
			if(result.isPresent()){
				try {
					int iterations = Integer.parseInt(result.get());
					timerIndex = 0;
					Timeline timer = new Timeline(
							new KeyFrame(Duration.millis(250),
								new EventHandler<ActionEvent>() {
									@Override
									public void handle(ActionEvent event) {
										morph.onlyBorder();
										addImage();
										morph.dilation();
										addImage();
										timerIndex++;
										if(timerIndex == iterations){
											morph.onlyBorder();
											addImage();
										}
											
									}
								}));
					timer.setCycleCount(iterations);
					timer.play();
				} catch (NumberFormatException ex) {
					System.out.println("fasche Zahl");
				}

			}
			
		});
		btGenerateLandscape.setTooltip(new Tooltip("Generiert mithilfe von R�ndern und Dilatation eine prozedurale Landschaft"));
		btBack.setOnAction(e->{
			if(!stack.empty()){
				morph.setImage(stack.pop());
				imgView.setImage(morph.getImage());
			}
				
		});
	}
	private void addImage(){
		stack.push(morph.getImage());
		imgView.setImage(stack.peek());
	}
	public static void main(String[] args) {
		launch(args);
	}
}
