package de.ddkfm.application;

import java.util.Random;
import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

public class Morphing {
	private Image img;
	
	public Morphing() {
		loadDefaultImage();
	}
	public void setImage(Image img){
		this.img = img;
	}
	public Image getImage(){
		return img;
	}
	private void loadDefaultImage(){
		img  = new Image(getClass().getResource("default.bmp").toExternalForm());
	}
	public void generateRandomImage(double probabilityBlack){
		WritableImage wImg = new WritableImage(500, 500);
		PixelWriter writer = wImg.getPixelWriter();
		for(int x = 0;x<wImg.getWidth();x++)
			for(int y = 0;y<wImg.getHeight();y++){
				int randNumber = new Random().nextInt(100);
				if(randNumber <(probabilityBlack) )
					writer.setColor(x, y, Color.BLACK);
				else
					writer.setColor(x, y, Color.WHITE);
			}
		img = wImg;
	}
	public void dilation(){
		WritableImage wImg = new WritableImage((int)img.getWidth(),(int)img.getHeight());
		PixelWriter writer = wImg.getPixelWriter();
		PixelReader reader = img.getPixelReader();
		for(int x = 1;x<(wImg.getWidth()-1);x++)
			for(int y = 1;y<(wImg.getHeight()-1);y++){
				Color[] neighbor = new Color[8];
				neighbor[0] = reader.getColor(x-1, y-1);
				neighbor[1] = reader.getColor(x, y-1);
				neighbor[2] = reader.getColor(x+1, y-1);
				neighbor[3] = reader.getColor(x-1, y);
				neighbor[4] = reader.getColor(x+1, y);
				neighbor[5] = reader.getColor(x-1, y+1);
				neighbor[6] = reader.getColor(x, y+1);
				neighbor[7] = reader.getColor(x+1, y+1);
				Color currentColor = reader.getColor(x, y);
				if(currentColor.equals(Color.WHITE)){
					if(foundColor(neighbor, Color.BLACK) != -1)
						writer.setColor(x, y,Color.BLACK);
					else
						writer.setColor(x, y, Color.WHITE);
				}else
					writer.setColor(x, y, currentColor);
			}
		img = wImg;
	}
	public void erosion(){
		WritableImage wImg = new WritableImage((int)img.getWidth(),(int)img.getHeight());
		PixelWriter writer = wImg.getPixelWriter();
		PixelReader reader = img.getPixelReader();
		for(int x = 1;x<(wImg.getWidth()-1);x++)
			for(int y = 1;y<(wImg.getHeight()-1);y++){
				Color[] nearColor = new Color[8];
				nearColor[0] = reader.getColor(x-1, y-1);
				nearColor[1] = reader.getColor(x, y-1);
				nearColor[2] = reader.getColor(x+1, y-1);
				nearColor[3] = reader.getColor(x-1, y);
				nearColor[4] = reader.getColor(x+1, y);
				nearColor[5] = reader.getColor(x-1, y+1);
				nearColor[6] = reader.getColor(x, y+1);
				nearColor[7] = reader.getColor(x+1, y+1);
				Color currentColor = reader.getColor(x, y);
				if(currentColor.equals(Color.BLACK)){
					if(foundColor(nearColor, Color.WHITE) != -1)
						writer.setColor(x, y,Color.WHITE);
					else
						writer.setColor(x, y, Color.BLACK);
				}else
					writer.setColor(x, y, currentColor);
			}
		img = wImg;
	}
	public void onlyBorder(){
		WritableImage wImg = new WritableImage((int)img.getWidth(),(int)img.getHeight());
		PixelWriter writer = wImg.getPixelWriter();
		PixelReader reader = img.getPixelReader();
		for(int x = 1;x<(wImg.getWidth()-1);x++)
			for(int y = 1;y<(wImg.getHeight()-1);y++){
				Color[] nearColor = new Color[8];
				nearColor[0] = reader.getColor(x-1, y-1);
				nearColor[1] = reader.getColor(x, y-1);
				nearColor[2] = reader.getColor(x+1, y-1);
				nearColor[3] = reader.getColor(x-1, y);
				nearColor[4] = reader.getColor(x+1, y);
				nearColor[5] = reader.getColor(x-1, y+1);
				nearColor[6] = reader.getColor(x, y+1);
				nearColor[7] = reader.getColor(x+1, y+1);
				Color currentColor = reader.getColor(x, y);
				if(currentColor.equals(Color.BLACK)){
					if(foundColor(nearColor, Color.WHITE) != -1)
						writer.setColor(x, y,Color.BLACK);
					else
						writer.setColor(x, y, Color.WHITE);
				}else
					writer.setColor(x, y, currentColor);
			}
		img = wImg;
	}
	
	private static int foundColor(Color[] array, Color c){
		int found = -1;
		int i = 0;
		while(i<array.length && (found == -1)){
			found = array[i].equals(c)?i:-1;
			i++;
		}
		return found;
	}
	
}
